# Multivariate regression analysis for population-level metagenomics

This folder contains source R code for the multivariate regression analysis used in the study. 


**Population-level metagenomics uncovers distinct effects of multiple medications on the human gut microbiome**  
Nagata and Nishijima et al 2022, Gastroenterology

# Usage
Rscript multivariate_regression.R `[microbial profile]` `[metadata file]` `[threshold for low-abundant taxonomy]` `[pseud value to add]` `[output dir]`  
Please also see an example in the example.sh.  


Example files are available in the data directory which were obtained from Zeller et al in the curatedMetagenomicData.


**Potential of fecal microbiota for early-stage detection of colorectal cancer**  
Zeller et al 2014, Mol Syst Biol


**Accessible, curated metagenomic data through ExperimentHub**  
Pasolli et al 2017, Nature Methods


# Dependencies
tidyverse (https://cran.r-project.org/web/packages/tidyverse/index.html)  
vegan (https://cran.r-project.org/web/packages/vegan/index.html)  
glm2 (https://cran.r-project.org/web/packages/glm2/index.html)  
