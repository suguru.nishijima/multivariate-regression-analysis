## usage
## Rscript regression.R [microbial profile] [metadata file] [threshold for low-abundant taxonomy] [pseud value to add] [output dir]

Rscript multivariate_regression.R data/example.ZellerG_2014.tsv data/example.ZellerG_2014_metadata.tsv 0.005 0.0001 output
